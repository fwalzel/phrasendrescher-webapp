<?php 
// ------------ RESPONSE.PHP --------------- //
/* 
FW: Diese Datei erh�lt �ber eine JSON-Ajax Schnittstelle in unserer iOS Phrasendrescher-APP eine Anfrage, ein JSON-File zur�ckzugeben.
Per GET wird die Varibale "file" �bergeben, die den Dateinamen der auszuliefernden JSON-Datei im gleichen Verzeichnis wie response.php
benennt. Je nach Verlauf werden dazu Logfiles in logs/ geschrieben.  
*/

//error_reporting(E_ALL);
header('Content-Type: application/json; charset=utf-8');

if(isset($_GET["file"])) {
	
	$filename = $_GET["file"].".json";

	if (file_exists($filename)) {
		if (is_readable($filename)) {	
				$myjson = file_get_contents($filename);	
				$success = true;
			}
			else {	$myjson = array( "error" => "File is not readable"); }
		}
		else {	$myjson = array( "error" => "File does not exist"); }
}
else {	$myjson = array( "error" => "No variable 'file' passed (method:get)"); }

$mydate = getdate();

if ($success) {
	
	// -- aktuelles JSON-File zur�ckgeben
	echo $myjson;
	
	// -- Monatliches Logfile schreiben
	$monthlyLog = "logs/logfile_".$mydate['year']."-".$mydate['mon'].".php";
	
	if (file_exists ($monthlyLog)) {
	
		$log = file_get_contents($monthlyLog);
		$chain = explode(" Nr: ",$log);
		$nr = intval(end($chain))+1;
		$log .= "\nZugriff am: ".$mydate['year']."-".$mydate['month']."-".$mydate['mday']." um: ".$mydate['hours'].":".$mydate['minutes'].":".$mydate['seconds']." Nr: ".$nr;	
	}
	else {
		$log = "<?php defined('_PEXEC090') or die('Restricted access'); ?>\n\n";
		$log .= "---------- LOG DATA ----------\n\n";
		$log .= "Zugriff am: ".$mydate['year']."-".$mydate['month']."-".$mydate['mday']." um: ".$mydate['hours'].":".$mydate['minutes'].":".$mydate['seconds']." Nr: 1";
	}
	file_put_contents($monthlyLog, $log);
	
	
	// -- Absolutes Logfile schreiben (Gesamtzugriffe)
	$totalLog = "logs/total_access_log.php";
	
	if (file_exists ($totalLog)) {
		$log = file_get_contents($totalLog);
		$chain = explode(" ---> ",$log);
		$nr = intval(end($chain))+1;
		$log = $chain[0]." ---> ".$nr;
	}
	else {
		$log = "<?php defined('_PEXEC090') or die('Restricted access'); ?>\n\n";
		$log .= "Gesamtzahl der Zugriffe seit dem ".$mydate['year']."-".$mydate['month']."-".$mydate['mday']." ab ".$mydate['hours'].":".$mydate['minutes'].":".$mydate['seconds']." Uhr ---> 1";
	}
	file_put_contents($totalLog, $log);
}
else {
	// -- Fehlermeldung zur�ckgeben
	echo json_encode($myjson);
	
	// -- Error Logfile schreiben
	$errorLog = "logs/error_log.php";
	
	if (file_exists ($errorLog)) {
		$log = file_get_contents($errorLog);
		$chain = explode(" Nr: ",$log);
		$nr = intval(end($chain))+1;
		$log .= "\nFehlerhafter Zugriff am: ".$mydate['year']."-".$mydate['month']."-".$mydate['mday']." um: ".$mydate['hours'].":".$mydate['minutes'].":".$mydate['seconds'];
		$log .= " Fehlertyp: '".$myjson['error']."' Nr: ".$nr;	
	}
	else {
		$log = "<?php defined('_PEXEC090') or die('Restricted access'); ?>\n\n";
		$log .= "---------- ERROR DATA ----------\n\n";
		$log .= "Fehlerhafter Zugriff am: ".$mydate['year']."-".$mydate['month']."-".$mydate['mday']." um: ".$mydate['hours'].":".$mydate['minutes'].":".$mydate['seconds'];
		$log .= " Fehlertyp: '".$myjson['error']."' Nr: 1";
	}
	file_put_contents($errorLog, $log);
}
?>